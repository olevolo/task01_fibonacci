/*
 *  Copyright(s) 2018 Olevolo, Inc
 *
 *  This software is the confidential
 *  and proprietary information of me.
 */
package com.epam.olevolo;

import com.epam.olevolo.lib.FibonacciSequence;
import com.epam.olevolo.lib.Interval;
import java.util.Scanner;

/**
 * Class for interaction with user.
 *
 * @version 1.4 11 November 2018
 * @author Volodymyr Oleksiuk
 */
public class ConsoleGUI {

    /** Scanner. */
    private Scanner scanner;

    /** Interval. */
    private Interval interval;

    /** FibonacciSequence. */
    private FibonacciSequence fibonacciSequence;

    /** Constructor. */
    public ConsoleGUI() {
        this.scanner = new Scanner(System.in);
    }

    /** Main loop. */
    public final void run() {
        System.out.println("Enter beginning of the interval [a, b]:");
        validate("a = ", "\\d+");
        int a = scanner.nextInt();

        System.out.println("Now, end of the interval [a, b]:");
        validate("b = ", "\\d+");
        int b = scanner.nextInt();

        interval = new Interval(a, b);
        fibonacciSequence = new FibonacciSequence(
                interval.getMaxOdd(),
                interval.getMaxEven(),
                FibonacciSequence.DEFAULT);

        menu();
    }

    /** Menu. */
    public final void menu() {
        System.out.println();
        System.out.println("Hello, my friend!");
        System.out.println("Choose an option:");
        System.out.println(
                  "1. Print odd numbers from start to the end of interval\n"
                + "2. Print even numbers from end to start of interval\n"
                + "3. Print the sum of odd and even numbers\n"
                + "4. Build and print Fibonacci sequence\n"
                + "5. Print percentage of odd and even Fibonacci numbers\n"
                + "6. Set another interval\n"
                + "7. Exit program"
                );
        validate("Your choice: ", "[1-7]");
        makeChoice(scanner.nextLine());
    }

    /** Validates input from scanner. */
    private void validate(final String message, final String pattern) {
        System.out.print(message);
        while (!scanner.hasNext(pattern)) {
            scanner.next();
            System.out.print(message);
        }
    }

    /** Makes choice. */
    public final void makeChoice(final String choice) {
        switch (choice) {
            case "1":
                interval.printOddNumbers();
                menu();
                break;
            case "2":
                interval.printEvenNumbers();
                menu();
                break;
            case "3":
                interval.printSumEvenOdd();
                menu();
                break;
            case "4":
                int fibSeqSize = setFibSeqSize();
                fibonacciSequence = new FibonacciSequence(
                        interval.getMaxOdd(),
                        interval.getMaxEven(),
                        fibSeqSize);
                fibonacciSequence.print();
                menu();
                break;
            case "5":
                fibonacciSequence.showPercentage();
                menu();
                break;
            case "6":
                run();
                break;
            case "7":
                exit();
                break;
            default:
                System.out.println("Something happened!");
                break;
        }
    }

    /** Gets n of FibSeq from user. */
    public final int setFibSeqSize() {
        System.out.print("Enter how many Fibonacci nums u want to get: ");
        while (!scanner.hasNextInt()) {
            System.out.print("Enter how many Fibonacci nums u want to get: ");
            scanner.next();
        }
        return scanner.nextInt();
    }

    /** Exits. */
    public final void exit() {
        scanner.close();
        System.exit(0);
    }
}
