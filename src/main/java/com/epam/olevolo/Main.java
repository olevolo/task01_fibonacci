/*
 *  Copyright(s) 2018 Olevolo, Inc
 *
 *  This software is the confidential
 *  and proprietary information of me.
 */
package com.epam.olevolo;

/**
 * Entry point of program.
 * Here the things begin.
 *
 * @version 1.0 12 November 2018
 * @author Volodymyr Oleksiuk
 */
public class Main {

    /**
     *  Method main documentation comment.
     *
     * @param args parameters from the command line
     */
    public static void main(String[] args) {
        ConsoleGUI gui = new ConsoleGUI();
        gui.run();
    }
}
