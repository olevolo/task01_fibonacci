/*
 *  Copyright(s) 2018 Olevolo, Inc
 *
 *  This software is the confidential
 *  and proprietary information of me.
 */
package com.epam.olevolo.lib;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for working with Fibonacci numbers.
 *
 * @version 1.0 12 November 2018
 * @author Volodymyr Oleksiuk
 */
public class FibonacciSequence {

    /** Default value for size of Fibonacci sequence. */
    public static final int DEFAULT = 10;

    /** f1. */
    private int f1;

    /** f2. */
    private int f2;

    /** Value for size of Fibonacci sequence. */
    private int n;

    /** Fibonacci sequence itself. */
    private List<Integer> fibSeq;

    /**
     * Constructor Interval documentation comment.
     */
    public FibonacciSequence(final int f1, final int f2, final int n) {
        this.f1 = f1;
        this.f2 = f2;
        this.n = n;

        /* order first two number in sequence if not*/
        if (this.f1 > this.f2) {
            int ftemp = this.f1;
            this.f1 = this.f2;
            this.f2 = ftemp;
        }
        buildSequence();
    }

    /** Builds Fibonacci sequence. */
    private void buildSequence() {
        fibSeq = new ArrayList<Integer>();
        if (n == 0) {
            return;
        }
        if (n == 1) {
            fibSeq.add(f1);
            return;
        }
        fibSeq.add(f1);
        fibSeq.add(f2);

        for (int i = fibSeq.size(); i < n; i++) {
            fibSeq.add(fibSeq.get(i - 1) + fibSeq.get(i - 2));
        }
    }

    /** Prints Fibonacci sequence. */
    public final void print() {
        for (Integer i : fibSeq) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     *  Calculates and shows Fibonacci sequence
     *  odd and even numbers percentage.
     */
    public final void showPercentage() {
        int oddCounter = 0;
        int evenCounter = 0;
        double hundredPercent = 100.0;

        for (Integer i : fibSeq) {
            if (i % 2 == 0) {
                evenCounter++;
            } else {
                oddCounter++;
            }
        }
        double oddPercentage =
                (double) oddCounter / fibSeq.size() * hundredPercent;
        double evenPercentage =
                (double) evenCounter / fibSeq.size() * hundredPercent;
        System.out.println("Percentage of odd numbers: " + oddPercentage);
        System.out.println("Percentage of even numbers: " + evenPercentage);
    }
}
