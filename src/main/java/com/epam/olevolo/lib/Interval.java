/*
 *  Copyright(s) 2018 Olevolo, Inc
 *
 *  This software is the confidential
 *  and proprietary information of me.
 */
package com.epam.olevolo.lib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class describes the interval of integer numbers.
 * Interval contains beginning and end boundary and
 * stores odd and even numbers in ArrayList<Integer>.
 * Can perform operations with them.
 *
 * @version 1.4 11 November 2018
 * @author Volodymyr Oleksiuk
 */
public class Interval {

    /** Left boundary of interval.*/
    private int begin;

    /** Right boundary of interval.*/
    private int end;

    /** Contains all odd numbers in the interval.*/
    private List<Integer> oddNumbers;

    /** Contains all even numbers in the interval.*/
    private List<Integer> evenNumbers;

    /**
     * Constructor Interval documentation comment.
     */
    public Interval(final int begin,final int end) {
        this.begin = begin;
        this.end = end;
        oddNumbers = new ArrayList<>();
        evenNumbers = new ArrayList<>();
        setOddAndEvenNumbers();
    }

    /** Fills lists with appropriate numbers.*/
    private void setOddAndEvenNumbers() {
        for (int i = begin; i <= end; i++) {
            if (isEven(i)) {
                evenNumbers.add(i);
            } else {
                oddNumbers.add(i);
            }
        }
        Collections.reverse(evenNumbers);
    }

    /** Check for parity. */
    public final boolean isEven(final int number) {
        return number % 2 == 0;
    }

    /** Gets maximum even number. */
    public final int getMaxEven() {
        return isEven(end) ? end : end - 1;
    }

    /** Gets maximum odd number. */
    public final int getMaxOdd() {
        return !isEven(end) ? end : end - 1;
    }

    /** Prints odd numbers from interval in ascending order. */
    public final void printOddNumbers() {
        System.out.println("Odd numbers from interval in ascending order:");
        for (Integer i : oddNumbers) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /** Prints even numbers from interval in descending order. */
    public final void printEvenNumbers() {
        System.out.println("Even numbers from interval in descending order:");
        for (Integer i : evenNumbers) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /** Finds and prints sum of even and odd numbers from interval. */
    public final void printSumEvenOdd() {
        int evenSum = 0;
        int oddSum = 0;
        System.out.print("Sum of even numbers: ");
        for (Integer i : oddNumbers) {
            evenSum += i;
        }
        System.out.println(evenSum);
        System.out.print("Sum of odd numbers numbers: ");
        for (Integer i : oddNumbers) {
            oddSum += i;
        }
        System.out.println(oddSum);
    }
}
